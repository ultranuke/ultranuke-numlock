﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UltraNuke.Utility;

namespace UltraNuke.NumLock
{
    public partial class MainForm : BaseForm
    {
        private HookUtil hook;
        private Dictionary<Keys, string> keyList;
        private bool formClose;

        public MainForm()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.numlock_32;
            this.Text = String.Format("{0} V{1}", AssemblyTitle, AssemblyVersion);
            this.notifyIcon1.Icon = this.Icon;

            hook = new HookUtil();
            keyList = new Dictionary<Keys, string>();
            keyList.Add(Keys.U, "4");
            keyList.Add(Keys.I, "5");
            keyList.Add(Keys.O, "6");
            keyList.Add(Keys.J, "1");
            keyList.Add(Keys.K, "2");
            keyList.Add(Keys.L, "3");
            keyList.Add(Keys.M, "0");
            this.hook.KeyDown += new KeyEventHandler(hook_KeyDown);
        }

        void hook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11 && Control.ModifierKeys == Keys.Shift)
            {
                this.TopMost = true;
                this.Show();
            }

            if (Properties.Settings.Default.IsNumLock)
            {
                if (keyList.ContainsKey(e.KeyCode))
                {
                    SendKeys.Send(keyList[e.KeyCode]);
                    e.Handled = true;
                }
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.IsNumLock = checkBox1.Checked;
            Properties.Settings.Default.Save();
            this.Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.hook.InstallHook();
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!formClose)
            {
                this.WindowState = FormWindowState.Minimized;
                e.Cancel = true;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void 打开数字小键盘ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void 关于我们ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutusForm form = new AboutusForm();
            form.ShowDialog(this);
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formClose = true;
            this.hook.UnInstallHook();
            Application.Exit();
        }
    }
}
