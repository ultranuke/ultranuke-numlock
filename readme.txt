﻿纽客数字小键盘 Version 1.0.0.0
Copyright(c) 2013, ultranuke.com. All rights reserved.

联系QQ：2816914883
联系QQ群：291908953
联系邮件：ultranuke@gmail.com
官方博客：http://www.cnblogs.com/ultranuke

系统简介
    纽客数字小键盘是纽客软件专门针对部分没有提供数字小键盘功能的笔记本电脑而设计开发的辅助功能软件，通过快捷键Shift+F11弹出是否启用小键盘设置窗口。开启数字小键盘后，按字母uiojklm键将分别对应显示4561230数字。


版本历史：
Version 1.0.0.0 - 2013年01月18日
    初始版本发布